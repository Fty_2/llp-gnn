import pandas as pd
import numpy as np
from torch.utils.data import Dataset
import torch
from functions import *



def get_csv_data(fileName, dataIdStart=0, nData=-1):
    '''
    Get data from csv file.
    Input: file_name, total_data_number
    Output: scalars([n_nodes,1])  vectors([n_nodes,2,4]) event_id([n_nodes]) (start from 1, not 0) 
    '''
    data = pd.read_csv(fileName, index_col=False).set_index('event id', drop=False)
    data_unique_id = data['event id'].unique()

    begin = data_unique_id[dataIdStart]
    datasize = len(data_unique_id) - dataIdStart
    nData = nData if (nData>0 and nData<datasize) else datasize
    end = data_unique_id[nData - 1]
    
    # if datasetType=='train':
    #     begin = data_unique_id[0]
    #     end = data_unique_id[0+ int(3/5*nData) - 1]
    # elif datasetType=='test':
    #     begin = data_unique_id[int(3/5*nData)]
    #     end = data_unique_id[int(4/5*nData) - 1]
    # elif datasetType=='validate':
    #     begin = data_unique_id[int(4/5*nData)]
    #     end = data_unique_id[nData - 1]
    
    data = data.loc[begin : end]
    event_id = torch.tensor(data.index)
    
    x = np.array(data[['T','X','Y','Z']]).reshape(1,-1,4)
    # x[0,:,0] = x[0,:,0] / 3e2
    p = np.array(data[['E','PX','PY','PZ']]).reshape(1,-1,4)
    # p[0,:,0] = p[0,:,0] / 3e2
    vectors = torch.tensor( np.concatenate((x,p),axis=0), dtype=torch.float32).transpose(0,1)
    # scalars = normsq4(vectors[:,0,:]).reshape(-1,1) / 3e2
    scalars = torch.zeros([len(vectors), 1])
    return scalars, vectors, event_id
    
    
    

class MyDataset(Dataset):
    def __init__(self, fileList, labelList, dataIdStart=0, nData = -1):
        # scalars([n_nodes,1])  vectors([n_nodes,2,4]) event_id([n_nodes]) (start from 1, not 0)
        self.scalars = torch.zeros(0,1)
        self.vectors = torch.zeros(0,2,4)
        self.event_id = torch.zeros(0)
        self.labels = torch.zeros(0)
        for id in range(len(fileList)):
            tmps, tmpv, tmpid = get_csv_data(fileList[id], dataIdStart, nData)
            if id>0:
                tmpid = tmpid + self.event_id[-1] + 1
            
            self.scalars = torch.cat([self.scalars, tmps], dim=0)
            self.vectors = torch.cat([self.vectors, tmpv], dim=0)
            self.event_id = torch.cat([self.event_id, tmpid], dim=0)
            self.labels = torch.cat([self.labels, torch.ones(len(tmps)) * labelList[id]])
            
        self.event_unique_id = torch.unique(self.event_id)
    
    def __getitem__(self, index):        
        indexes_for_index = ((self.event_id == self.event_unique_id[index].reshape(-1,1)).nonzero(as_tuple=False))
        event_ids = indexes_for_index[:,0]
        indexes_for_index = indexes_for_index[:,1]
        
        
        event_nodes_num = unsorted_segment_sum( torch.ones(len(event_ids), dtype=torch.int).reshape(-1,1), event_ids, len(event_ids.unique()) ).reshape(-1)
        # indexes_for_index = (self.event_id == self.event_unique_id[index]).nonzero().reshape(-1)
        
        # output: scalars([n_nodes,1])  vectors([n_nodes,2,4]) label([n_nodes])  event_nodes_num([n_events])
        return self.scalars[indexes_for_index], self.vectors[indexes_for_index], self.labels[indexes_for_index], event_nodes_num
        # return torch.zeros(self.scalars[indexes_for_index].shape), self.vectors[indexes_for_index], self.labels[indexes_for_index], event_nodes_num
        
    def __len__(self):
        return len(self.event_unique_id)
        
        