import matplotlib.pyplot as plt

def drawLoss(train_losses, test_losses, saveFig = ""):
    plt.figure(figsize=(10,5))
    plt.title("TrainLoss and TestLoss During Training")
    plt.plot(train_losses,label="train")
    plt.plot(test_losses,label="test")
    plt.xlabel("iterations")
    plt.ylabel("Loss")
    plt.legend()
    plt.show()
    if saveFig != "":
        plt.savefig(saveFig)
    

def drawAcc(train_accs, test_accs, saveFig = ""):
    plt.figure(figsize=(10,5))
    plt.title("TrainAcc and TestAcc During Training")
    plt.plot(train_accs,label="train")
    plt.plot(test_accs,label="test")
    plt.xlabel("iterations")
    plt.ylabel("Accuracy")
    plt.legend()
    plt.show()
    if saveFig != "":
        plt.savefig(saveFig)


# def __name__=="__main__":
#     drawLoss(res['train_loss'], res['test_loss'])
#     drawAcc(res['train_acc'], res['test_acc'] )