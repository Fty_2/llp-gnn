import torch
from torch import nn
import torchvision.transforms as transforms
from functions import *

class LGEB(nn.Module):
    def __init__(self, n_input, n_output, n_hidden, n_node_attr=0,
                 dropout = 0., c_weight=1.0, last_layer=False):
        super(LGEB, self).__init__()
        self.c_weight = c_weight
        n_edge_attr = 6 # dims for Minkowski norm & inner product

        self.phi_e = nn.Sequential(
            nn.Linear(n_input * 2 + n_edge_attr, n_hidden, bias=False),
            nn.BatchNorm1d(n_hidden),
            nn.ReLU(),
            nn.Linear(n_hidden, n_hidden),
            nn.ReLU())

        self.phi_h = nn.Sequential(
            nn.Linear(n_hidden + n_input + n_node_attr, n_hidden),
            nn.BatchNorm1d(n_hidden),
            nn.ReLU(),
            nn.Linear(n_hidden, n_output))

        layer = nn.Linear(n_hidden, 1, bias=False)
        torch.nn.init.xavier_uniform_(layer.weight, gain=0.001)

        self.phi_x = nn.Sequential(
            nn.Linear(n_hidden, n_hidden),
            nn.ReLU(),
            layer)

        self.phi_m = nn.Sequential(
            nn.Linear(n_hidden, 1),
            nn.Sigmoid())
        
        self.last_layer = last_layer
        if last_layer:
            del self.phi_x

    def m_model(self, hi, hj, x_norms, x_dots, p_norms, p_dots, xipj_dots, pixj_dots):
        out = torch.cat([hi, hj, x_norms, x_dots, p_norms, p_dots, xipj_dots, pixj_dots], dim=1)
        out = self.phi_e(out)
        w = self.phi_m(out)
        out = out * w
        return out

    def h_model(self, h, edges, m, node_attr):
        i, j = edges
        agg = unsorted_segment_sum(m, i, num_segments=h.size(0))
        agg = torch.cat([h, agg, node_attr], dim=1)
        out = h + self.phi_h(agg)
        return out

    def x_model(self, x, edges, x_diff, m):
        i, j = edges
        trans = x_diff * self.phi_x(m)
        # From https://github.com/vgsatorras/egnn
        # This is never activated but just in case it explosed it may save the train
        trans = torch.clamp(trans, min=-100, max=100)
        agg = unsorted_segment_mean(trans, i, num_segments=x.size(0))
        x = x + agg * self.c_weight
        return x

    def minkowski_feats(self, edges, v1, v2, if_biDirection=False):
        i, j = edges
        diff = v1[i] - v2[j]
        norms = normsq4(diff).unsqueeze(1)
        dots = dotsq4(v1[i], v2[j]).unsqueeze(1)
        norms, dots = psi(norms), psi(dots)
        
        if if_biDirection:
            inverse_dots = psi( dotsq4(v2[i], v1[j]).unsqueeze(1) )
            return norms, dots, diff, inverse_dots
        else:
            return norms, dots, diff

    def forward(self, h, x, p, edges, node_attr=None):
        i, j = edges
        x_norms, x_dots, x_diff = self.minkowski_feats(edges, x, x) #[nEdges, -1]
        p_norms, p_dots, p_diff = self.minkowski_feats(edges, p, p) #[nEdges, -1]
        _, xipj_dots, _, pixj_dots = self.minkowski_feats(edges, x, p, True) #[nEdges, -1]

        m = self.m_model(h[i], h[j], x_norms, x_dots, p_norms, p_dots, xipj_dots, pixj_dots) # [nEdges, hidden]
        if not self.last_layer:
            x = self.x_model(x, edges, x_diff, m)
            p = self.x_model(p, edges, p_diff, m)
        h = self.h_model(h, edges, m, node_attr)
        return h, x, p, m

    
    
class LorentzNet(nn.Module):
    r''' Implimentation of LorentzNet.
    Args:
        - `n_scalar` (int): number of input scalars.
        - `n_hidden` (int): dimension of latent space.
        - `n_class`  (int): number of output classes.
        - `n_layers` (int): number of LGEB layers.
        - `c_weight` (float): weight c in the x_model.
        - `dropout`  (float): dropout rate.
    '''
    def __init__(self, n_scalar, n_hidden, n_class = 2, n_layers = 6, c_weight = 1e-3, dropout = 0.):
        super(LorentzNet, self).__init__()
        self.n_scalar = n_scalar
        self.n_hidden = n_hidden
        self.n_layers = n_layers
        self.bnVectors =  nn.BatchNorm1d(8)
        self.bnScalars = nn.BatchNorm1d(self.n_scalar)
        self.embedding = nn.Linear(n_scalar, n_hidden)
        self.LGEBs = nn.ModuleList([LGEB(self.n_hidden, self.n_hidden, self.n_hidden, 
                                    n_node_attr=n_scalar, dropout=dropout,
                                    c_weight=c_weight, last_layer=(i==n_layers-1))
                                    for i in range(n_layers)])
        self.graph_dec = nn.Sequential(nn.Linear(self.n_hidden, self.n_hidden),
                                       nn.ReLU(),
                                       nn.Dropout(dropout),
                                       nn.Linear(self.n_hidden, n_class)) # classification

    def forward(self, scalars, vectors, edges, node_mask, batch_size):
        # scalars([n_nodes,1])  vectors([n_nodes,2,4])  edges([2,n_edges])  node_mask([n_nodes]  event id for each node)
        # vectors =self.bnVectors(vectors.reshape(-1,8)).reshape(-1,2,4)
        h = scalars
        # h = self.bnScalars(scalars)
        h = self.embedding(h)  # h: [B*N, n_input]
        x, p = torch.transpose(vectors, 0,1)
        
        for i in range(self.n_layers):
            h, x, p, _ = self.LGEBs[i](h, x, p, edges, node_attr=scalars)
            
        h = unsorted_segment_mean(h, node_mask, num_segments=batch_size)
#         h = h * node_mask
#         h = h.view(-1, n_nodes, self.n_hidden)
#         h = torch.mean(h, dim=1)
        # h: [batch_size, n_hidden]
        pred = self.graph_dec(h)
        return pred.squeeze(1)