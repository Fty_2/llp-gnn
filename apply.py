import torch
from torch import nn
import torchvision.transforms as transforms
from torch.utils.data import Dataset
import torch.optim as optim
import argparse, json
import sys

from load_config import *
from train_test import *
from model import LorentzNet
from dataset import MyDataset
from analysis import drawAcc, drawLoss
import random
import os


def seed_everything(seed):
    """
    Seeds basic parameters for reproductibility of results
    
    Arguments:
        seed {int} -- Number of the seed
    """
    random.seed(seed)
    os.environ["PYTHONHASHSEED"] = str(seed)
    np.random.seed(seed)
    torch.manual_seed(seed)
    torch.cuda.manual_seed(seed)
    torch.backends.cudnn.deterministic = True
    torch.backends.cudnn.benchmark = False


#Usage: python main.py config.json

if __name__=="__main__":
    seed_everything(42)
    device = torch.device("cuda:0" if torch.cuda.is_available() else "cpu")
    
    net = torch.load(logDir + '/net.pt',map_location=torch.device('cpu'))
    net.to(device)

    # Loss function 
    criterion = nn.CrossEntropyLoss(reduction='sum')

    # datasets & data_id_loader
    dataset = MyDataset(fileList, labelList, dataIdStart=data_id_start, nData=num_data)
    nData = len(dataset)

    shuffled_id = np.arange(nData)
    np.random.shuffle(shuffled_id)


    val_id_loader = shuffled_id.copy()
    val_id_loader.resize(int(len(val_id_loader) / batch_size), batch_size )
           
            
    # Validation
    valres = {'test_time': [], 'test_loss': [], 'test_acc': [],
        'test_n_bkg': [], 'test_bkg_eff': [], 'test_n_sig': [], 'test_sig_eff': []}
    scoresLog = test_one_epoch(net, dataset, val_id_loader, criterion, valres)

    for classid in range(n_classes):
        np.save(logDir+'/scoreApply'+str(classid)+'.npy', arr=scoresLog[classid])

    print("\n\n")
    print("Validation finished.")
    print("Validation time %.2f" % ( valres['test_time'][-1]))
    print("Validation loss: %.4f \t Validation acc: %.4f" % (valres['test_loss'][-1], valres['test_acc'][-1]))
    print("Bkg cut eff:", valres['test_bkg_eff'][-1], " for n_bkg: ",valres['test_n_bkg'][-1])
    print("Sig cut eff:", valres['test_sig_eff'][-1], " for n_bkg: ",valres['test_n_sig'][-1])



    




    