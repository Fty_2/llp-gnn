import numpy as np
import sys
import matplotlib.pyplot as plt
from sklearn.metrics import roc_curve
from sklearn.metrics import auc

def cut4(score, x1, y1, x2, y2, x3, y3): 
    passCut = score[ ~((score[:,0]/x1 + score[:,1]/y1>1) & (score[:,1]/y2+score[:,0]/x2>1) &
                         (score[:,1]/y3+score[:,0]/x3>1)) ]
    if len(score)!=0:
        print("Cut efficiency:", len(passCut), "/", len(score), "=", len(passCut)/len(score))
    return len(passCut), len(score)

def drawCut4(xpoint1, ypoint1, xpoint2, ypoint2, xpoint3, ypoint3):
    x1 = np.linspace(1e-13, xpoint1, 100)
    y1 = (1 - x1 / xpoint1) * ypoint1
    x2 = np.linspace(1e-13, xpoint2, 100)
    y2 = (1 - x2 / xpoint2) * ypoint2
    x3 = np.linspace(1e-13, xpoint3, 100)
    y3 = (1 - x3 / xpoint3) * ypoint3
    plt.plot(x1,y1,'black',zorder=2)
    plt.plot(x2,y2,color='black',zorder=2)
    plt.plot(x3,y3,color='black',zorder=2)


if __name__=='__main__':

    if len(sys.argv) != 3:
        print("usage: python drawScore.py logDirMainName classNum")
        exit()

    classNum = int(sys.argv[2])
    logDirectory = sys.argv[1]

    if classNum==3:
        x1, y1, x2, y2, x3, y3 = 1e-13, 1e-13,  1e-1,  1e-1, 1e-13, 1e-13
        # x1, y1, x2, y2, x3, y3 = 1e-1, 2e-5, 1e-13, 1e-13, 1e-13, 1e-13 
        x2, y2 = 1.5e-4, 1.5e-3
        print(f"Cut: x1:{x1}, y1:{y1}; x2:{x2}, y2:{y2}; x3:{x3}, y3:{y3}")
        print(f"Cut: x:{x2}, y:{y2}")
        passCut, rawNum = [], []

        allScore = np.zeros([0,2])
        for classid in range(classNum):
            score = np.load(logDirectory+'/scoreApply'+str(classid)+'.npy')

            # allScore = np.concatenate((allScore, score[:,0:2]),0)
            c0score = score[:,0]
            c1score = score[:,1]

            print("In class", classid, ":",end="")
            tmppass, total = cut4(score, x1, y1, x2, y2, x3, y3)
            passCut.append(tmppass)
            rawNum.append(total)

            if classid==0:
                plt.figure(classNum, dpi=500)
                plt.loglog(c0score, c1score, '.', color='blue',alpha=0.005,zorder=3, label="background")

                plt.figure(classNum + 1, dpi=500)
                plt.loglog(c0score, c1score, '.', color='blue', zorder=3, markersize=1, label="background")
            elif classid==1:
                plt.figure(classNum, dpi=500)
                plt.loglog(c0score, c1score, '.', color='blue',alpha=0.005,zorder=3)

                plt.figure(classNum + 1, dpi=500)
                plt.loglog(c0score, c1score, '.', color='blue', zorder=3, markersize=1)

            else:
                plt.figure(classNum, dpi=500)
                plt.loglog(c0score, c1score, '.', color='red',alpha=0.005,zorder=1, label="signal")
                plt.figure(classNum + 1, dpi=500)
                plt.loglog(c0score, c1score, '.', color='red', zorder=1, markersize=1, label="signal")

        sigpass, sigtotal = 0,0
        for isig in range(2, len(passCut)):
            sigpass += passCut[isig]
            sigtotal += rawNum[isig]
        print()
        print("Sig efficiency:", sigpass, "/", sigtotal, "=", sigpass/sigtotal)


        plt.figure(classNum, dpi=500)
        drawCut4(x1, y1, x2, y2, x3, y3)
        plt.axis([1e-13,2,1e-13,2])  
        plt.legend()
        plt.xlabel("class0 score")
        plt.ylabel("class1 score")
        plt.savefig(logDirectory+"allDensityWithCut.png")

        plt.figure(classNum + 1, dpi=500)
        drawCut4(x1, y1, x2, y2, x3, y3)
        plt.axis([1e-13,2,1e-13,2])  
        plt.legend()
        plt.xlabel("class0 score")
        plt.ylabel("class1 score")
        plt.savefig(logDirectory+"allPointsWithCut.png")

    elif classNum==2:
        color=['blue','red']
        label=['background','signal']
        # x = 0.9 
        # print(f"Cut: x:{x}")
        passCut, rawNum = [], []
        allScore, allLabel = np.zeros(0), np.zeros(0)
 
        
        plt.figure(dpi=500)
        plt.yscale('log')
        for classid in range(classNum):
            score = np.load(logDirectory+'/scoreApply'+str(classid)+'.npy')[:,1]
            allScore = np.concatenate((allScore, score),0)
            allLabel = np.concatenate((allLabel, np.ones(len(score))*classid),0)
 
            # print("In class", classid, ":",end="")
 
            # passCut.append( (score>x).sum() )
            # rawNum.append(len(score))
            plt.hist(score,bins=100,histtype='step',edgecolor=color[classid],label=label[classid])

            # print("Cut efficiency:", passCut[-1], "/", rawNum[-1], "=", passCut[-1]/rawNum[-1])
        
        # plt.axvline(x=x,linestyle='--',label='cut',color='black')
        plt.legend(loc='upper center')
        plt.xlabel("score")
        plt.ylabel("Num")
        plt.savefig(logDirectory+"histWithCut.png")

        fpr, tpr, cut = roc_curve(allLabel, allScore)
        tnr = 1-fpr
        roc_auc = auc(tnr, tpr)
        
        plt.figure(dpi=500)
        plt.plot(tnr,tpr, 'k--', label='ROC (area = {0:.4f})'.format(roc_auc), lw=2)
        plt.xlim([-0.05,1.05])
        plt.ylim([-0.05,1.05])
        plt.xlabel('True Negative Rate')
        plt.ylabel('True Positive Rate')
        plt.title("ROC Curve")
        plt.legend(loc="lower left")
        plt.show()
        plt.savefig(logDirectory+'/roc.png')
        


