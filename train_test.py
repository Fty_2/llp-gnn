import time
import torch
from torch import nn
import torchvision.transforms as transforms
from torch.utils.data import Dataset
import torch.optim as optim
from functions import get_edges_nodemask_labels
from load_config import *


device = torch.device("cuda:0" if torch.cuda.is_available() else "cpu")
def train_one_epoch(model, dataset, data_id_loader, criterion, optimizer, res):
    model.train()
    
    timeBegin = time.time()
    total_n_data, total_correct, total_loss = 0, 0, 0
    # get data_index from data_id_loader, then obatain data from dataset
    for data_idx in data_id_loader:
        scalars, vectors, labels, event_nodes_num = dataset[data_idx]
        
        edges, node_mask, labels = get_edges_nodemask_labels(event_nodes_num, labels)
        labels = labels.type(torch.LongTensor)
        scalars, vectors, edges, node_mask, labels = scalars.to(device), vectors.to(device), edges.to(device), node_mask.to(device), labels.to(device)

        bs = batch_size
        # clear the gradients of all optimized variables
        optimizer.zero_grad()
        # forward pass: compute predicted outputs by passing inputs to the model
        # scalars([n_nodes,1])  vectors([n_nodes,2,4])  edges([2,n_edges])  node_mask([n_nodes]  event id for each node)
        output = model(scalars, vectors, edges, node_mask, bs)
        # calculate the batch loss

        loss = criterion(output, labels)
        # backward pass: compute gradient of the loss with respect to model parameters
        loss.backward()
        optimizer.step()
        
        # Calculate Accuracy
        output = nn.Softmax(dim=1)(output)
        score, predict = torch.max(output.detach(),1)
        correct = (predict==labels).sum().item()
        
        total_n_data += len(output)
        total_correct += correct
        total_loss += loss.item()

    # record result
    print(" labels: ", labels[0:5])
    print(" output: ", output[0:5, :])
    
    print("num_events: ", total_n_data)
    print()
    res['train_time'].append(time.time()-timeBegin)
    res['train_loss'].append(float(total_loss/total_n_data))
    res['train_acc'].append(float(total_correct/total_n_data))
    

def test_one_epoch(model, dataset, data_id_loader, criterion, res, check_output = False):
    model.eval()
    
    timeBegin = time.time()
    scoresLog = []
    for classid in range(n_classes):
        scoresLog.append(np.zeros([0, n_classes]))   

    total_n_data, total_correct, total_loss = 0, 0, 0
    # get data_index from data_id_loader, then obatain data from dataset
    for data_idx in data_id_loader:
        with torch.no_grad():
            scalars, vectors, labels, event_nodes_num = dataset[data_idx]

            edges, node_mask, labels = get_edges_nodemask_labels(event_nodes_num, labels)

            labels = labels.type(torch.LongTensor)
            scalars, vectors, edges, node_mask, labels = scalars.to(device), vectors.to(device), edges.to(device), node_mask.to(device), labels.to(device)
            bs = batch_size

            # forward pass: compute predicted outputs by passing inputs to the model
            # scalars([n_nodes,1])  vectors([n_nodes,2,4])  edges([2,n_edges])  node_mask([n_nodes]  event id for each node)
            output = model(scalars, vectors, edges, node_mask, bs)
            # calculate the batch loss
            loss = criterion(output, labels)
            # backward pass: compute gradient of the loss with respect to model parameters

            # Calculate Accuracy
            output = nn.Softmax(dim=1)(output)
            score, predict = torch.max(output.detach(),1)
            correct = (predict==labels).sum().item()
            
            for classid in range(n_classes):
                scoreForI = output[labels == classid]
                scoreForI = scoreForI.detach().cpu().numpy()
                scoresLog[classid] = np.concatenate((scoresLog[classid],scoreForI))

            total_n_data += len(output)
            total_correct += correct
            total_loss += loss.item()
            
    if check_output:
        print(" labels: ", labels[0:5])
        print(" output: ", output[0:5, :])

    n_bkg, passCut = 0, 0
    for classid in range(n_classes-1):
        scoreForI = scoresLog[classid]
        n_bkg += len(scoreForI)
        passCut += len(scoreForI[ (scoreForI[:,0] + scoreForI[:,1]<0.1) ])
    res['test_n_bkg'].append(n_bkg)
    res['test_bkg_eff'].append(passCut/n_bkg)
    
    sigScore = scoresLog[-1]
    passCut = len(sigScore[ (sigScore[:,0] + sigScore[:,1]<0.1) ])
    res['test_n_sig'].append(len(sigScore))
    res['test_sig_eff'].append(passCut/len(sigScore))



    # record result
    print("num_events: ", total_n_data)
    print()
    res['test_time'].append(time.time()-timeBegin)
    res['test_loss'].append(float(total_loss/total_n_data))
    res['test_acc'].append(float(total_correct/total_n_data))
    
    return scoresLog
