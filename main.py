import torch
from torch import nn
import torchvision.transforms as transforms
from torch.utils.data import Dataset
import torch.optim as optim
import argparse, json
import sys
import json

from load_config import *
from train_test import *
from model import LorentzNet
from dataset import MyDataset
from analysis import drawAcc, drawLoss
import random
import os


def seed_everything(seed):
    """
    Seeds basic parameters for reproductibility of results
    
    Arguments:
        seed {int} -- Number of the seed
    """
    random.seed(seed)
    os.environ["PYTHONHASHSEED"] = str(seed)
    np.random.seed(seed)
    torch.manual_seed(seed)
    torch.cuda.manual_seed(seed)
    torch.backends.cudnn.deterministic = True
    torch.backends.cudnn.benchmark = False


#Usage: python main.py config.json

if __name__=="__main__":
    print("start")
    seed_everything(42)
    device = torch.device("cuda:0" if torch.cuda.is_available() else "cpu")


    if pretrained_dir != None:
        net = torch.load(pretrained_dir + '/net.pt',map_location=torch.device('cpu'))
        # Record result
        with open(pretrained_dir+'/train-result.json','r') as f:
            res = json.load(f)
        epoch_start = res['epochs'][-1]+1
    else:
        net = LorentzNet(1,n_hidden=n_hidden,n_class=n_classes, n_layers=n_layers)
        # Record result
        res = {'epochs': [], 'lr' : [], 'train_time': [], 'test_time': [], 
            'train_loss': [], 'test_loss': [], 'train_acc': [], 'test_acc': [],
            'test_n_bkg': [], 'test_bkg_eff': [], 'test_n_sig': [], 'test_sig_eff': []}
        epoch_start = 1
    net.to(device)

    # Loss function & optimizer
    criterion = nn.CrossEntropyLoss(reduction='sum')
    optimizer = optim.Adam(net.parameters(), lr=lr)

    # datasets & data_id_loader
    dataset = MyDataset(fileList, labelList, dataIdStart=data_id_start, nData=num_data)
    nData = len(dataset)

    shuffled_id = np.arange(nData)
    np.random.shuffle(shuffled_id)

    train_id_loader_orig = shuffled_id[0: int(nData * 3 / 5)]
    # train_id_loader = np.arange(int(nData * 3 / 5))
    # tmpid = np.arange( int(len(train_id_loader) / batch_size) * batch_size ).reshape(-1,batch_size)
    # train_id_loader = train_id_loader[tmpid]

    test_id_loader = shuffled_id[int(nData * 3 / 5):int(nData * 4 / 5)].copy()
    test_id_loader.resize(int(len(test_id_loader) / batch_size), batch_size )
    # test_id_loader = np.arange(int(nData * 3 / 5), int(nData * 4 / 5))
    # np.random.shuffle(test_id_loader)
    # tmpid = np.arange( int(len(test_id_loader) / batch_size) * batch_size ).reshape(-1,batch_size)
    # test_id_loader = test_id_loader[tmpid]

    val_id_loader = shuffled_id[int(nData * 4 / 5): int(nData)].copy()
    np.save(logDir+'/valdata_id.npy', arr=val_id_loader)
    val_id_loader.resize(int(len(val_id_loader) / batch_size), batch_size )
    # val_id_loader = np.arange(int(nData * 4 / 5), int(nData))
    # np.random.shuffle(val_id_loader)
    # tmpid = np.arange( int(len(val_id_loader) / batch_size) * batch_size ).reshape(-1,batch_size)
    # val_id_loader = val_id_loader[tmpid]

    # Loop epoch 
    for epoch in range(epoch_start, epoch_start + num_epochs):
        if len(lr_list)==num_epochs:
            optimizer = optim.Adam(net.parameters(), lr=lr_list[epoch-epoch_start])
            
        res['epochs'].append(epoch)
        res['lr'].append(optimizer.param_groups[0]['lr'])


        np.random.shuffle( train_id_loader_orig )
        train_id_loader = train_id_loader_orig.copy()
        train_id_loader.resize(int(len(train_id_loader) / batch_size), batch_size )
        # train_id_loader = np.arange(int(nData * 3 / 5))
        # tmpid = np.arange( int(len(train_id_loader_orig) / batch_size) * batch_size ).reshape(-1,batch_size)
        # train_id_loader = train_id_loader_orig[tmpid]
        

        train_one_epoch(net, dataset, train_id_loader, criterion, optimizer, res)
        
        tmp = {'test_time': [], 'test_loss': [], 'test_acc': [],
            'test_n_bkg': [], 'test_bkg_eff': [], 'test_n_sig': [], 'test_sig_eff': []}
        test_one_epoch(net, dataset, train_id_loader, criterion, tmp, check_output = False)
        res['train_loss'][-1] = tmp['test_loss'][-1]
        res['train_acc'][-1] = tmp['test_acc'][-1]
        
        scoresLog = test_one_epoch(net, dataset, test_id_loader, criterion, res, check_output = True)
        
        for classid in range(n_classes):
            np.save(logDir+'/scoreApply'+str(classid)+'.npy', arr=scoresLog[classid])

        json_object = json.dumps(res, indent=4)
        with open(f"{logDir}/train-result.json", "w") as outfile:
            outfile.write(json_object)

        torch.save(net, logDir + '/net.pt',_use_new_zipfile_serialization=False)
        print("Epoch %d/%d finished." % (epoch, num_epochs))
        print("Train time: %.2f \t Test time %.2f" % (res['train_time'][-1], res['test_time'][-1]))
        print("Train loss %.4f \t Train acc: %.4f" % (res['train_loss'][-1], res['train_acc'][-1]))
        print("Test loss: %.4f \t Test acc: %.4f" % (res['test_loss'][-1], res['test_acc'][-1]))
        print("Bkg cut eff:", res['test_bkg_eff'][-1], " for n_bkg: ",res['test_n_bkg'][-1])
        print("Sig cut eff:", res['test_sig_eff'][-1], " for n_bkg: ",res['test_n_sig'][-1])
        print("\n\n", flush=True)
        
        # Save training log
        drawLoss(res['train_loss'], res['test_loss'], logDir+"loss.png")
        drawAcc(res['train_acc'], res['test_acc'], logDir+'acc.png')
            
            
    # Validation
    valres = {'test_time': [], 'test_loss': [], 'test_acc': [],
        'test_n_bkg': [], 'test_bkg_eff': [], 'test_n_sig': [], 'test_sig_eff': []}
    scoresLog = test_one_epoch(net, dataset, val_id_loader, criterion, valres)

    for classid in range(n_classes):
        np.save(logDir+'/scoreApply'+str(classid)+'.npy', arr=scoresLog[classid])

    print("\n\n")
    print("Validation finished.")
    print("Validation time %.2f" % ( valres['test_time'][-1]))
    print("Validation loss: %.4f \t Validation acc: %.4f" % (valres['test_loss'][-1], valres['test_acc'][-1]))
    print("Bkg cut eff:", valres['test_bkg_eff'][-1], " for n_bkg: ",valres['test_n_bkg'][-1])
    print("Sig cut eff:", valres['test_sig_eff'][-1], " for n_bkg: ",valres['test_n_sig'][-1])




    




    