import torch
import scipy.sparse as sp

def unsorted_segment_sum(data, segment_ids, num_segments):
    r'''Custom PyTorch op to replicate TensorFlow's `unsorted_segment_sum`.
    Adapted from https://github.com/vgsatorras/egnn.
    '''
    result = data.new_zeros((num_segments, data.size(1)))
    result.index_add_(0, segment_ids, data)
    return result

def unsorted_segment_mean(data, segment_ids, num_segments):
    r'''Custom PyTorch op to replicate TensorFlow's `unsorted_segment_mean`.
    Adapted from https://github.com/vgsatorras/egnn.
    '''
    result = data.new_zeros((num_segments, data.size(1)))
    count = data.new_zeros((num_segments, data.size(1)))
    result.index_add_(0, segment_ids, data)
    count.index_add_(0, segment_ids, torch.ones_like(data))
    return result / count.clamp(min=1)

def normsq4(p):
    r''' Minkowski square norm
         `\|p\|^2 = p[0]^2-p[1]^2-p[2]^2-p[3]^2`
    ''' 
    psq = torch.pow(p, 2)
    return 2 * psq[..., 0] - psq.sum(dim=-1)
    
def dotsq4(p,q):
    r''' Minkowski inner product
         `<p,q> = p[0]q[0]-p[1]q[1]-p[2]q[2]-p[3]q[3]`
    '''
    psq = p*q
    return 2 * psq[..., 0] - psq.sum(dim=-1)
    
def psi(p):
    ''' `\psi(p) = Sgn(p) \cdot \log(|p| + 1)`
    '''
    return torch.sign(p) * torch.log(torch.abs(p) + 1)



def get_edges_nodemask_labels(event_nodes_num, node_labels):
    node_mask = torch.zeros([0], dtype=torch.long)
    labels = torch.zeros([len(event_nodes_num)], dtype=torch.int)
    
    sparse_matrix = torch.zeros([len(node_labels), len(node_labels)], dtype=torch.long) - torch.eye(len(node_labels), dtype=torch.long)
    total_nodes = 0
    for ievent in range(len(event_nodes_num)):
        n_nodes = event_nodes_num[ievent]
        total_nodes += n_nodes
        
        node_mask = torch.cat( [node_mask, ievent* torch.ones(n_nodes, dtype=torch.int)], dim=0 )
        labels[ievent] = node_labels[total_nodes - 1]
        sparse_matrix[total_nodes-n_nodes:total_nodes, total_nodes-n_nodes:total_nodes] += 1
        
    ed = sp.coo_matrix(sparse_matrix)
    edges = torch.cat( [torch.tensor(ed.row, dtype=torch.long).reshape(1,-1), torch.tensor(ed.col, dtype=torch.long).reshape(1,-1)])
    return edges, node_mask, labels
        


