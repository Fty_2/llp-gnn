import sys
import json
import numpy as np


if len(sys.argv) != 2:
    print("Usage: python main.py config.json")
    exit()

configFile = sys.argv[1]
with open(configFile, "r") as f:
    config = json.load(f)


fileList = config["fileList"]
labelList = config['labelList']
n_classes = len( np.unique(labelList) )

batch_size = config['batch_size']

num_epochs = config['num_epochs']

lr = config['lr']

lr_list = [] if config.get('lr_list')==None else config.get('lr_list')

logDir = config['logDir']

data_id_start = 0 if config.get('data_id_start')==None else config.get('data_id_start')

num_data = config['nData']

# Set LorentzNet parameters
n_hidden = 10 if config.get('n_hidden')==None else config.get('n_hidden')

n_layers = 10 if config.get('n_layers')==None else config.get('n_layers')

# If there is pre-training
pretrained_dir = config.get('pretrained_dir')